﻿using CountApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace CountApp.DataContext
{
    public interface ICounterContext : IDisposable
    {
        DbSet<Counter> Counters { get; }
        int SaveChanges();  
    }
}
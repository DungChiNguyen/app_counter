﻿using CountApp.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CountApp.DataContext
{
    public class CounterContext : DbContext, ICounterContext
    {
        public CounterContext()
            : base("name=DefaultConnection")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<CounterContext>());
        } 
        public DbSet<Counter> Counters { get; set; }
         
    }
}
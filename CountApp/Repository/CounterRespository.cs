﻿using CountApp.DataContext;
using CountApp.Models;
using CountApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Linq.Expressions;

namespace CountApp.Repository
{
    /// <summary>
    /// implement detail function for particular repository
    /// </summary>
    public class CounterRespository : ICounterRespository
    { 
        private readonly ICounterContext context;

        public CounterRespository()
        {
            context = new CounterContext();
        }
        
        public CounterRespository(ICounterContext counterContext)
        {
            context = counterContext;
        }

        public void Add(Counter entity)
        {
            context.Counters.Add(entity); 
        }

        public Counter GetFirstValue()
        {
            return context.Counters.FirstOrDefault();
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
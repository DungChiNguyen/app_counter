﻿using CountApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace CountApp.Repository
{
    /// <summary>
    /// Using to declare general function to apply into child repository.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IGenericRepository<T> where T : class
    { 
        void Add(T entity); 
        void Save();
    }
}
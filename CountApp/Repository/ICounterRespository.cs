﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using CountApp.Models;

namespace CountApp.Repository
{
    /// <summary>
    /// Using to declare particular business function for each repository
    /// Define structure function to support unit test
    /// </summary>
    public interface ICounterRespository : IGenericRepository<Counter>
    { 
        Counter GetFirstValue();
    }
}
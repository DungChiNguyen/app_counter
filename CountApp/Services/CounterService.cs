﻿using CountApp.Models;
using CountApp.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CountApp.Services
{
    /// <summary>
    /// Implement detail logical business Counter application function
    /// </summary>
    public class CounterService : ICounterService
    {
        private readonly ICounterRespository _counterRespository;

        public CounterService()
        {
            _counterRespository = new CounterRespository();
        }

        public CounterService(ICounterRespository counterRespository)
        {
            _counterRespository = counterRespository;
        }

        public Counter GetCurrentValue()
        { 
            return _counterRespository.GetFirstValue() ?? new Counter() { Number = 0 };
        }

        public int IncreaseValue()
        {
            try
            {
                Counter counter = GetCurrentValue();
                if (counter.Number == 0)
                {  
                    _counterRespository.Add(counter); 
                }

                if (counter.Number < 10)
                {
                    counter.Number++;
                    _counterRespository.Save();
                }
                return counter.Number; 
            }
            catch (Exception ex)
            { 
                throw ex;
            }
        }
    }
}
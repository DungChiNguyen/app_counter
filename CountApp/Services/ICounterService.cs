﻿using CountApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CountApp.Services
{
    /// <summary>
    /// Declare business function which using for counter applicaton 
    /// </summary>
    public interface ICounterService
    {
        int IncreaseValue();
        Counter GetCurrentValue();
    }
}
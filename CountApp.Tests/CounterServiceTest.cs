﻿using CountApp.Models;
using CountApp.Repository;
using CountApp.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CountApp.Tests
{
    [TestFixture]
    public class CounterServiceTest
    {
        [TestCase(0, true)]    // Testcase is applied when application start first times.
        [TestCase(11, false)]  // Testcase is applied to test number is not greater than 10.
        public void GetCurrentValueTest(int inputValue, bool result)
        { 
            Mock<ICounterRespository> mock = new Mock<ICounterRespository>();
            mock.Setup(x => x.GetFirstValue()).Returns(new Counter() { Number = inputValue });
            CounterService counterService = new CounterService(mock.Object);
            Counter resultNumber = counterService.GetCurrentValue();
            Assert.AreEqual(resultNumber.Number <=10, result);  
        }

        [TestCase(0,1 )]  // Testcase is applied when application start first times.
        [TestCase(5,6)]   // Testcase is applied when application start next times.
        [TestCase(11,11)] // Testcase is applied to test number is not greater than 10.
        public void IncreaseValueTest(int valueBeforeIncreasing, int valueAfterIncreasing)
        {
            Mock<ICounterRespository> mockCounterRespository = new Mock<ICounterRespository>();
            mockCounterRespository.Setup(x => x.GetFirstValue()).Returns(new Counter() { Number = valueBeforeIncreasing });
            mockCounterRespository.Setup(x => x.Add(It.IsAny<Counter>()));
            mockCounterRespository.Setup(x => x.Save());

            CounterService counterService = new CounterService(mockCounterRespository.Object);
            int resultNumber = counterService.IncreaseValue();
            Assert.AreEqual(resultNumber, valueAfterIncreasing);
        }
    }
}
